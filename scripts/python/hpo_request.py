"""
Description:
This code fetches HPO (Human Phenotype Ontology) terms associated with 
specific genes from an online database and saves them in a tab-separated 
file.

This Python script: 
Queries the Human Phenotype Ontology (HPO) database for genes you provide.
Extracts relevant ontology IDs for each gene, representing phenotypic features.
Loops through a list of deafness-related genes, retrieving their associated HPO terms.
Saves the gene-phenotype mappings in a tab-delimited file for further analysis.
Ultimately, it links genes to potential clinical features from a medical database.

Author: keith.graham5@nhs.net

"""
import requests
import csv

base_url = "https://hpo.jax.org/app/browse/"

def create_hpo_request(gene_symbol):
    url = base_url + "search?" + "q=" + gene_symbol
    print(url)

    hpo_list = []

    response = requests.get(url)

    if response.status_code == 200:
        try:
            gene_name_response = response.json()
            
            # Check if 'genes' key exists and is not empty in the response.
            if 'genes' in gene_name_response and gene_name_response['genes']:
                gene_id = gene_name_response['genes'][0]['geneId']
                gene_url = base_url + "gene/" + str(gene_id)
                
                gene_response = requests.get(gene_url)
                gene_json = gene_response.json()
                
                term_json = gene_json.get('termAssoc', [])
                
                for term in term_json:
                    hpo_list.append(term['ontologyId'])
        except requests.exceptions.JSONDecodeError as e:
            print(f"Error decoding JSON: {e}")
    else:
        print(f"Error: {response.status_code}")

    return hpo_list

deafness_gene_list = ['XIAP', 'WFS1', 'USP7', 'AIFM1']

gene_hpo_list = []

for gene_symbol in deafness_gene_list:
    # for gene_symbol in gene_symbols:
    hpo_terms = create_hpo_request(gene_symbol)
    gene_hpo_list.append((gene_symbol, hpo_terms))

output_file = 'gene_hpo_terms.tsv'

with open(output_file, 'w', newline='') as tsvfile:
    writer = csv.writer(tsvfile, delimiter='\t')
    writer.writerow(['Gene Symbol', 'HPO Terms'])
    writer.writerows(gene_hpo_list)
